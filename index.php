<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="slick/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="slick/slick-1.6.0/slick/slick.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="slick/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="slick/slick-1.6.0/slick/slick-theme.css">  
        <script src="js_slider.js" type="text/javascript"></script>
        <link href="css.css" rel="stylesheet" type="text/css"/>
        <script src="jquery.validate.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="contenedor">
            <div class="your-class" id="slide">
                <img id="img" src="Stewie.png"/>
                <img id="img" src="Peter_Griffin.png"/>
                <img id="img" src="brian-griffin.png"/>
            </div>
        </div>
        <form id="formulari1" action="result.php" method="post" autocomplete="off">
            <div class="contenedor" id="jugadores">
                <div class="p" id="player1">
                    <div id="cambcolor1" onclick="cambiarColor(1);" class="boton">JUGADOR 1</div><br>
                    Direccion : <input type="text" name="direccion1" placeholder="direccion"/><br>
                    Edad: <input type="number" name="edad1"/><br>
                    Personaje <select name="sel1" id="persona1" onchange="agregarIMG(1)">
                        <option value="" disabled selected>Seleccione Opcion</option>
                        <option value="stewie1">Stewie</option>
                        <option value="peter1">Peter</option>
                        <option value="brian1">Brian</option>
                    </select>
                    <div class="imagenes" id="img1a"><img src="Stewie.png"></div>
                    <div class="imagenes" id="img1b"><img src="Peter_Griffin.png"></div>
                    <div class="imagenes" id="img1c"><img src="brian-griffin.png"></div>
                </div>
                <div class="p" id="player2">
                    <div id="cambcolor2" onclick="cambiarColor(2);" class="boton" >JUGADOR 2</div><br>
                    Direccion : <input type="text" name="direccion2" placeholder="direccion"/><br>
                    Edad: <input type="number" name="edad2"/><br>
                    Personaje <select name="sel2" id="persona2" onchange="agregarIMG(2)">
                        <option value="" disabled selected>Seleccione Opcion</option>
                        <option value="stewie2">Stewie</option>
                        <option value="peter2">Peter</option>
                        <option value="brian2">Brian</option>
                    </select>
                    <div class="imagenes" id="img2a"><img src="Stewie.png"></div>
                    <div class="imagenes" id="img2b"><img src="Peter_Griffin.png"></div>
                    <div class="imagenes" id="img2c"><img src="brian-griffin.png"></div>
                </div>
            </div>
            <input type="submit" name="FIGHT" value="FIGHT">
        </form>

    </body>
</html>
