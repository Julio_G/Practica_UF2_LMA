<?php
$numganador = rand(1, 2);

echo "El ganador es el jugador número $numganador";


if ($numganador == 1) {
    $addr = $_POST["direccion1"];
    $edad = $_POST["edad1"];
    ?>
    <br><audio controls preload="auto">
        <source src="codiGmap3Audio/bensoundcute.mp3" type="audio/mpeg">
        Audio no disponible.
    </audio>
    <?php
} else {
    $addr = $_POST["direccion2"];
    $edad = $_POST["edad2"];
    ?>
    <br><audio controls  preload="auto">
        <source src="codiGmap3Audio/bensoundepic.mp3" type="audio/mpeg">
        Audio no disponible.
    </audio>
    <?php
}
?>
<html>
    <head>
        <script src="codiGmap3Audio/jquery-3.1.1.min.js" type="text/javascript"></script>     
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoLdkq0ijaqaTW1o8k1mZUqg_YP8TuNx8&region=GB"></script>
        <script src="codiGmap3Audio/gmap3.min.js" type="text/javascript"></script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div id="mapa2" style="width: 500px; height:400px;"></div>
        <script>

            var contentString = '';
            $(document).ready(init);
            function init() {
                $('#mapa2')
                        .gmap3({
                            zoom: 10
                        })
                        .infowindow({content: "contentString"})
                        .marker([
                            {address: "<?php echo $addr ?>", data: "<?php echo "edad:$edad" ?>", icon: "http://maps.google.com/mapfiles/marker_grey.png"}

                        ])
                        .on('click', function (marker) {  //Al clicar obrim una finestra sobre la marca i hi insertem el data de la marca
                            marker.setIcon('http://maps.google.com/mapfiles/marker_green.png');
                            var map = this.get(0); //this.get(0) retorna la primera propietat vinculada-> gmap3
                            var infowindow = this.get(1); //this.get(1) retorna la segona propietat vinculada -> infowindow
                            infowindow.setContent(marker.data);     //dins la finestra mostrem el atribut data de la marca
                            infowindow.open(map, marker);
                        })
                        .fit();
            }
        </script>
    </body>
</html>