$(document).ready(inicio);

function inicio() {
    
    $('.your-class').slick({
        /*autoplay: true,*/
        dots: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
            breakpoint:700,
            settings: {
                slidesToShow: 1,
                slidesToScroll : 1,
                dots: true
            }
        }
        ]
    });
    
    $("#formulari1").validate({
        focusCleanup: true,
        rules: {
            direccion1: {
                required: true,
                minlength: 5,
                maxlength: 20
            },
            edad1: {
                required: true,
                min: 18,
                max: 99
            },
            per1: {
                required: true
            },
            direccion2: {
                required: true,
                minlength: 5,
                maxlength: 20
            },
            edad2: {
                required: true,
                min: 18,
                max: 99
            },
            per2: {
                required: true
            }
        },
        messages: {
            direccion1: {
                required: "<br>(Este campo es obligatorio)",
                minlength: "<br>(Inserta al menos 5 letras)",
                maxlength: "<br>(Máximo 20 letras)"
            },
            edad1: {
                required: "<br>(Este campo es obligatorio)",
                min: "<br>(Minimo 18 años)",
                max: "<br>(Te recomendamos no pelear)"
            },
            per1: {
                required: "<br>(Este campo es obligatorio)"
            },
            direccion2: {
                required: "<br>(Este campo es obligatorio)",
                minlength: "<br>(Inserta al menos 5 letras)",
                maxlength: "<br>(Máximo 20 letras)"
            },
            edad2: {
                required: "<br>(Este campo es obligatorio)",
                min: "<br>(Minimo 18 años)",
                max: "<br>(Te recomendamos no pelear)"
            },
            per2: {
                required: "(Este campo es obligatorio)"
            }
        }
    });
    $("#formulari1").valid();
    $(".imagenes").fadeOut();
    
}

var color = 'red';
function cambiarColor(x) {
    var colour= 0;
    console.log(x);
    if(color == 'red'){
        color = 'blue';
    }else if(color == 'blue'){
        color = 'green';
    }else if(color == 'green'){
        color = 'red';
    }
    
    if(x === 1){
        document.getElementById('cambcolor1').style.background= color ;
    }else if(x === 2){
        document.getElementById('cambcolor2').style.background= color;
    }

}
function agregarIMG(x) {
    var nombre = "persona"+x;
    var selectBox = document.getElementById(nombre);
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    if (selectedValue === "peter"+x){
        $("#img"+x+"c").fadeOut();
        $("#img"+x+"a").fadeOut();
        $("#img"+x+"b").fadeIn();
    }
    else if (selectedValue === "stewie"+x){
        $("#img"+x+"b").fadeOut();
        $("#img"+x+"c").fadeOut();
        $("#img"+x+"a").fadeIn();
    }
    else if (selectedValue === "brian"+x){
        $("#img"+x+"a").fadeOut();
        $("#img"+x+"b").fadeOut();
        $("#img"+x+"c").fadeIn();
    }
}